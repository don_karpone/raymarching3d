﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Algebra;

namespace RayMarching3D
{
    interface IObject3D
    {
        System.Drawing.Color GetColor();
        bool isIntersected(MultiDimensionalVector A, MultiDimensionalVector B);
        double Distance(MultiDimensionalVector V);
        MultiDimensionalVector GetCenter();
    }

    class Sphere : IObject3D
    {
        public MultiDimensionalVector Center { get; }
        public double Radius { get; }

        Func<System.Drawing.Color> getColor;
        public System.Drawing.Color GetColor() => getColor();
        public Sphere(MultiDimensionalVector Center, double Radius, System.Drawing.Color color)
        {
            this.Center = Center;
            this.Radius = Radius;

            getColor = () => color;
        }

        public bool isIntersected(MultiDimensionalVector A, MultiDimensionalVector B)
            => Math.Sign(Distance(A)) != Math.Sign(Distance(A));

        public double Distance(MultiDimensionalVector V)
            => (V - Center).Abs - Radius;

        public MultiDimensionalVector GetCenter() => Center.Copy();

    }

    class Box : IObject3D
    {
        public MultiDimensionalVector Center { get; }
        public double Radius { get; }
        Func<System.Drawing.Color> getColor;
        public System.Drawing.Color GetColor() => getColor();

        public Box(MultiDimensionalVector Center, double Radius, System.Drawing.Color color)
        {
            this.Center = Center;
            this.Radius = Radius;

            getColor = () => color;
        }

        public bool isIntersected(MultiDimensionalVector A, MultiDimensionalVector B)
            => Math.Sign(Distance(A)) != Math.Sign(Distance(A));

        public double Distance(MultiDimensionalVector V)
        {
            MultiDimensionalVector Centred = V - Center;
            for (int i = 0; i < Centred.Dimensionality; i++)
                Centred[i] = Math.Abs(Centred[i]);
            Centred -= Radius;

            return new MultiDimensionalVector(
                Math.Max(Centred[0], 0), Math.Max(Centred[1], 0), Math.Max(Centred[2], 0)).Abs;
        }

        public MultiDimensionalVector GetCenter() => Center.Copy();

    }
}
