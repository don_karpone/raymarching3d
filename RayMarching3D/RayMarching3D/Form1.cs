﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Algebra;

namespace RayMarching3D
{
    public partial class Form1 : Form
    {
        Scene Scene;

        public Form1()
        {
            InitializeComponent();
            this.SizeChanged += Form1_SizeChanged;

            Scene = new Scene(new Bitmap(pictureBox1.Width, pictureBox1.Height));
            Draw();
        }

        private void Form1_SizeChanged(object sender, EventArgs e)
        {
            Scene.SetImage(new Bitmap((pictureBox1.Width < pictureBox1.Height) ? pictureBox1.Width : pictureBox1.Height,
                (pictureBox1.Width < pictureBox1.Height) ? pictureBox1.Width : pictureBox1.Height));
            Draw();
        }

        void Draw()
        {
            pictureBox1.Image = Scene.Draw();
            pictureBox1.Invalidate();
        }

    }
}
