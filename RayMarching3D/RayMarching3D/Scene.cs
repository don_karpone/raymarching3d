﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Algebra;
using System.Drawing;

namespace RayMarching3D
{
    class Scene
    {
        List<IObject3D> GameObjects;
        MultiDimensionalVector CameraLocation = new MultiDimensionalVector(0, 0, 30, 1);
        MultiDimensionalVector LightSourceLocation = new MultiDimensionalVector(0, -20, 30, 1);

        int RayCount = 256;
        double RayMaximumLength = 50;
        double CameraView = Math.PI / 6;

        double VerticalAngle = 0,
            HorizontalAngle = 0;

        double[][] RayLengths;
        Color[][] InheretedObjects;
        MultiDimensionalVector[][] RayStartLocations;
        double[] RayVerticalAngles;
        double[] RayHorizontalAngles;


        Bitmap scene;
        Graphics G;

        public Scene(Bitmap Canvas)
        {
            SetImage(Canvas);

            SetCameraAngles();
            InitializeObjects();
            SetRayAngles();
            SetRays();
        }
        public void SetImage(Bitmap Canvas)
        {
            this.scene = Canvas;
            G = Graphics.FromImage(Canvas);
        }
        void SetCameraAngles()
        {
            var len = Math.Sqrt(CameraLocation[0] * CameraLocation[0] + CameraLocation[1] * CameraLocation[1] + CameraLocation[2] * CameraLocation[2]);
            var xz = Math.Sqrt(CameraLocation[0] * CameraLocation[0] + CameraLocation[2] * CameraLocation[2]);
            HorizontalAngle = Math.Acos(CameraLocation[2] / xz);
            VerticalAngle = Math.Acos(xz / len);
        }

        void InitializeObjects()
        {
            //GameObjects = new List<IObject3D>{
            //    new Sphere(new MultiDimensionalVector(0, 0, 0, 1), 6, Color.Red),
            //    //new Box(new MultiDimensionalVector(-4, -2, 2.5, 1), 2, Color.Blue),
            //    new Sphere(new MultiDimensionalVector(-4, -2, 2.5, 1), 2, Color.Green),
            //    new Sphere(new MultiDimensionalVector(-4, 3.5, 5, 1), 2, Color.Green)};

            GameObjects = new List<IObject3D>{
                new Sphere(new MultiDimensionalVector(0, 0, 0, 1), 6, Color.PeachPuff),
                new Sphere(new MultiDimensionalVector(-3, 1.5, 3.5, 1), 2, Color.Red),
                new Sphere(new MultiDimensionalVector(-3, 1.5, 6.5, 1), 0.95, Color.Black),
                new Sphere(new MultiDimensionalVector(3, 1.5, 3.5, 1), 2, Color.Red),
                new Sphere(new MultiDimensionalVector(3, 1.5, 6.5, 1), 0.95, Color.Black)};
        }

        void SetRayAngles()
        {
            RayVerticalAngles = new double[RayCount];
            RayHorizontalAngles = new double[RayCount];

            for (int i = 0; i < RayCount; i++)
            {
                RayVerticalAngles[i] = CameraView / 2 - CameraView * i / (RayCount - 1) - VerticalAngle;
                RayHorizontalAngles[i] = CameraView / 2 - CameraView * i / (RayCount - 1) - HorizontalAngle;
            }
        }

        void SetRays()
        {
            RayLengths = new double[RayCount][];
            InheretedObjects = new Color[RayCount][];
            RayStartLocations = new MultiDimensionalVector[RayCount][];
            double MinDistance = GetMinDistance(CameraLocation);
            Task[] tasks = new Task[RayCount];

            for (int i = 0; i < RayCount; i++)
            {
                RayLengths[i] = new double[RayCount];
                RayStartLocations[i] = new MultiDimensionalVector[RayCount];
                InheretedObjects[i] = new Color[RayCount];
                tasks[i] = new Task(GetFunction(RayLengths[i], InheretedObjects[i], RayHorizontalAngles[i]));
                tasks[i].RunSynchronously();
            }
            Task.WaitAll(tasks);
        }

        Action GetFunction(double[] Rays, Color[] Inhereted, double HorizontalAngle)
        {
            return () =>
            {
                double tempDistance;
                MultiDimensionalVector loc = new MultiDimensionalVector(0,0,0,0);
                IObject3D closestObj = GameObjects.First();
                for (int i = 0; i < RayCount; i++)
                {
                    tempDistance = GetMinDistance(CameraLocation);
                    while (tempDistance > 0.1 && Rays[i] < RayMaximumLength)
                    {
                        Rays[i] += tempDistance;
                        loc = (Matrix.GetRotationMatrix3D(HorizontalAngle, Matrix.Axes.oY, true)
                            * Matrix.GetRotationMatrix3D(RayVerticalAngles[i], Matrix.Axes.oX, true)
                            * new MultiDimensionalVector(0, 0, -Rays[i], 1));
                        loc += CameraLocation;
                        (tempDistance, closestObj) = GetClosestObject(loc);
                    }

                    Rays[i] = (Rays[i] < RayMaximumLength) ? Rays[i] : RayMaximumLength;

                    loc = (Matrix.GetRotationMatrix3D(HorizontalAngle, Matrix.Axes.oY, true)
                            * Matrix.GetRotationMatrix3D(RayVerticalAngles[i], Matrix.Axes.oX, true)
                            * new MultiDimensionalVector(0, 0, -Rays[i], 1));
                    loc += CameraLocation;

                    Inhereted[i] = (Rays[i] < RayMaximumLength) ? SetColor(closestObj.GetColor(), Rays[i], loc, closestObj.GetCenter()) : Color.Black;

                }
            };

            Color SetColor(Color C, double Distance, MultiDimensionalVector Loc, MultiDimensionalVector secoundLoc)
            {
                //double c = 0.35;
                //return Color.FromArgb(
                //    (int)(c * C.R + (1 - c) * 255 * (1 - Distance / RayMaximumLength)),
                //    (int)(c * C.G + (1 - c) * 255 * (1 - Distance / RayMaximumLength)),
                //    (int)(c * C.B + (1 - c) * 255 * (1 - Distance / RayMaximumLength)));

                double c = GetAngleVectors(secoundLoc - Loc, LightSourceLocation - Loc);
                //return Color.FromArgb(
                //    (int)((1 - Distance / RayMaximumLength) * (Math.Pow(Math.Sin(c), 2) * C.R + Math.Pow(Math.Cos(c), 2) * 255)),
                //    (int)((1 - Distance / RayMaximumLength) * (Math.Pow(Math.Sin(c), 2) * C.G + Math.Pow(Math.Cos(c), 2) * 255)),
                //    (int)((1 - Distance / RayMaximumLength) * (Math.Pow(Math.Sin(c), 2) * C.B + Math.Pow(Math.Cos(c), 2) * 255)));

                return Color.FromArgb(
                    (int)((1 - Distance / RayMaximumLength) * (Math.Pow(Math.Cos(c), 2) * C.R)),
                    (int)((1 - Distance / RayMaximumLength) * (Math.Pow(Math.Cos(c), 2) * C.G)),
                    (int)((1 - Distance / RayMaximumLength) * (Math.Pow(Math.Cos(c), 2) * C.B)));
            }
        }

        public Bitmap Draw()
        {
            G.Clear(Color.Black);

            float StepX = (float)scene.Width / (RayCount - 1);
            float StepY = (float)scene.Height / (RayCount - 1);
            for (int i = 0, j; i < RayCount; i++)
                for (j = 0; j < RayCount; j++)
                {
                    G.FillRectangle(new SolidBrush(InheretedObjects[i][j]),
                        StepX * i, StepY * j, StepX, StepY);
                }
            return scene;
        }

        double GetMinDistance(MultiDimensionalVector Location) => GetClosestObject(Location, GameObjects).Distance;
        double GetMinDistanceWithException(MultiDimensionalVector Location, IObject3D exception) 
            => GetClosestObject(Location, GameObjects.FindAll((x) => x != exception)).Distance;

        (double Distance, IObject3D ClosestObject) GetClosestObject(MultiDimensionalVector Location) 
            => GetClosestObject(Location, GameObjects);

        (double Distance, IObject3D ClosestObject) GetClosestObject(MultiDimensionalVector Location, List<IObject3D> GameObjects)
        {
            double Min = double.MaxValue,
                Distance;

            IObject3D ClosestObj = GameObjects.First();
            foreach (IObject3D Obj in GameObjects)
            {
                Distance = Obj.Distance(Location);
                if (Distance < Min)
                {
                    Min = Distance;
                    ClosestObj = Obj;
                }
            }
            return (Min, ClosestObj);
        }

        double GetAngleVectors(MultiDimensionalVector A, MultiDimensionalVector B) => Math.Acos(Math.Round(MultiDimensionalVector.ScalarMultiplying(A, B) / (A.Abs * B.Abs), 4));
    }
}
